﻿using MailKit.Security;
using MimeKit;
using MailKit.Net.Smtp;
using DentalClinic.Notification.Infrastructure;
using DentalClinic.Common.Contracts.Notification;

namespace DentalClinic.Notification.Application
{
    public static class NotificationService
    {
        public static async Task SendAsync(MailSettings settings, NotificationRequest command)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(settings.Mail);
            email.To.Add(MailboxAddress.Parse(command.ToEmail));
            email.Subject = command.Subject;

            var builder = new BodyBuilder();
            builder.HtmlBody = command.Body;
            email.Body = builder.ToMessageBody();
            using var smtp = new SmtpClient();
            smtp.Connect(settings.Host, settings.Port, SecureSocketOptions.StartTls);
            smtp.Authenticate(settings.Mail, settings.Password);
            await smtp.SendAsync(email);
        }
    }
}
