using DentalClinic.Notification.Infrastructure;
using MassTransit;

namespace DentalClinic.Notification.Presentation
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            builder.Configuration.AddJsonFile("appsettings.json");

            builder.Services.Configure<MailSettings>(builder.Configuration.GetSection("MailSettings"));           

            builder.Services.AddMassTransit(x =>
            {
                x.AddConsumers(typeof(SendNotificationConsumer));

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(builder.Configuration["RabbitMQ:Host"], h =>
                    {
                        h.Username(builder.Configuration["RabbitMQ:Username"]);
                        h.Password(builder.Configuration["RabbitMQ:Password"]);
                    });
                    cfg.ConfigureEndpoints(context);
                });
            });

            var app = builder.Build();

            app.Run();
        }
    }
}