﻿using DentalClinic.Common.Contracts.Notification;
using DentalClinic.Notification.Application;
using DentalClinic.Notification.Infrastructure;
using MassTransit;
using Microsoft.Extensions.Options;

namespace DentalClinic.Notification.Presentation
{
    public class SendNotificationConsumer : IConsumer<NotificationRequest>
    {
        private readonly ILogger<NotificationRequest> _logger;
        private readonly IOptions<MailSettings> _mailSettings; 

        public SendNotificationConsumer(ILogger<NotificationRequest> logger, IOptions<MailSettings> mailSettings)
        {
            _logger = logger;
            _mailSettings = mailSettings;
        }

        public async Task Consume(ConsumeContext<NotificationRequest> context)
        {
            _logger.LogInformation("Email {email} Subject {subject} Body {body}",
                context.Message.ToEmail,
                context.Message.Subject,
                context.Message.Body);

            await NotificationService.SendAsync(_mailSettings.Value, context.Message);
        }
    }
}
